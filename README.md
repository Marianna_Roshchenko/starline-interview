Ниже представлены ссылки на репозитории проектов, которые разрабатывались с 2012 по 2014 г.
### Harmonic signal ###
* Год разработки: 2012 г.
* Язык: Matlab
* Репозиторий: [https://bitbucket.org/Marianna_Roshchenko/harmonic-sygnal](https://bitbucket.org/Marianna_Roshchenko/harmonic-sygnal)
* В папке [docs](https://bitbucket.org/Marianna_Roshchenko/harmonic-sygnal/src/012bd31f15a7219af4742ce7696bd42e75346fc8/docs/?at=master) находятся "Постановка задачи" и "Пояснительная записка" к выполненной задаче.

### Learn boost ###
* Год разработки: 2012 г.
* Основные языки: C#, HTML, CSS, JS
* Технологии: ASP.NET MVC, LINQ to SQL, Bootstrap, SQL Server
* Репозиторий: [https://bitbucket.org/Marianna_Roshchenko/learn-boost](https://bitbucket.org/Marianna_Roshchenko/learn-boost)
* В папке [scrins](https://bitbucket.org/Marianna_Roshchenko/learn-boost/src/818ccff03f53f37bbcb39ac015e5d8775a93c3fb/scrins/?at=master) находятся скриншоты сайта.
* [HTML код](https://bitbucket.org/Marianna_Roshchenko/learn-boost/src/818ccff03f53f37bbcb39ac015e5d8775a93c3fb/learnboost/Lab/Views/?at=master)
* [CSS код](https://bitbucket.org/Marianna_Roshchenko/learn-boost/src/818ccff03f53f37bbcb39ac015e5d8775a93c3fb/learnboost/Lab/Content/styles.css?at=master)
* [JS код](https://bitbucket.org/Marianna_Roshchenko/learn-boost/src/818ccff03f53f37bbcb39ac015e5d8775a93c3fb/learnboost/Lab/Scripts/app.js?at=master)
* [JS код](https://bitbucket.org/Marianna_Roshchenko/learn-boost/src/818ccff03f53f37bbcb39ac015e5d8775a93c3fb/learnboost/Lab/Scripts/app_stud.js?at=master)
### BusWeb ###
* Год разработки: 2012-2013 г.
* Репозиторий: [https://bitbucket.org/cityways/busweb/overview](https://bitbucket.org/cityways/busweb/overview)
* Основные языки: Java, HTML, CSS, JS, JQuery
* Технологии: SpringMVC, JSP, Google Maps API
### Logistic site ###
* Год разработки: 2014-2015 г.
* Основные языки: HTML, CSS, JS
* Технологии: Ruby on Rails
* Репозиторий: [https://bitbucket.org/Marianna_Roshchenko/logistic-site/overview](https://bitbucket.org/Marianna_Roshchenko/logistic-site/overview)